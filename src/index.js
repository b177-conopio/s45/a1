import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
// Import the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// const name = 'John Smith';
// const user = {
//   firstName: 'Jane',
//   lastName: 'Smith'
// }

// function formatName(name){
//   return user.firstName + ' ' + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// // to render element
// ReactDOM.render(
//     element,
//     //render whole element to id root
//     document.getElementById('root')
// );