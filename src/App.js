import React from 'react';
import { Fragment, useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom'
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './components/CourseView'
import Error from './pages/Error';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import './App.css';
import { UserProvider } from './UserContext';


function App() {
  // State hook for the user state that's defined for a global scope
  const [user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  // Function for clearing local sorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  // Used to check if the user info is properly stored during login and localStorage info is cleared upon logout
  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/courses" element={<Courses />} />
              <Route path="/courses/:courseId" element={<CourseView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />            
              <Route path="/logout" element={<Logout />} />
              <Route path="*" element={<Error />} />
            </Routes>
          </Container>
      </Router>
    </UserProvider> 
  );
}

export default App;


