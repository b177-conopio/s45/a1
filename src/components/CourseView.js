import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2';

export default function CourseView() {

	const { user } = useContext(UserContext);

	// Allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling
	const history = useNavigate();

	// The "useParams" hook allows us to retrive the courseId passed via the URL
	const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState(0);

	const enroll = (courseId) => {
		fetch('http://localhost:4000/users/enroll', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data){
				Swal.fire({
					title: "Successfully enrolled",
					icon: "success",
					text: "You have successfully enrolled for this course."
				})
				history("/courses");
			}
			else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	useEffect(()=> {

		console.log(courseId);

		fetch(`http://localhost:4000/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	}, [courseId]);


	return(
		<Container>
			<Row>
				<Card style={{ width: '18rem' }}>
				  <Card.Body className="text-center">
				    <Card.Title>{name}</Card.Title>
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>{description}</Card.Text>
				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>Php {price}</Card.Text>
				    <Card.Subtitle className="mb-2 text-muted">Class Schedule:</Card.Subtitle>
				    <Card.Text>8 am - 5 pm</Card.Text>
				    { user.id !== null ?
				    	<Button variant="primary" onClick={() => enroll(courseId)} block>Enroll</Button>
				    	:
				    	<Link className="btn btn-danger btn-block" to="/login">Log in to Enroll</Link>

				    }
				    
				  </Card.Body>
				</Card>
			</Row>
		</Container>
	)
}



// 	useEffect(() => {

// 		console.log(courseId);

// 		fetch(`http://localhost:4000/courses/${courseId}`)
// 		.then(res => res.json())
// 		.then(data => {
// 			console.log(data);

// 			setName(data.name);
// 			setDescription(data.description);
// 			setPrice(data.price);
// 		})

// 	}, [courseId]);

// 	return(
// 		<Container>
// 			<Row>
// 				<Card style={{ width: '18rem' }}>
// 				  <Card.Body className="text-center">
// 				    <Card.Title>{name}</Card.Title>
// 				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
// 				    <Card.Text>{description}</Card.Text>
// 				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
// 				    <Card.Text>Php {price}</Card.Text>
// 				    <Card.Subtitle className="mb-2 text-muted">Class Schedule:</Card.Subtitle>
// 				    <Card.Text>8 am - 5 pm</Card.Text>
// 					{/*onClick={() => enroll(courseId) is an callback function*/}
// 					{user.id !== null ?
// 						<Button variant="primary" onClick={() => enroll(courseId)} block>Enroll</Button>
// 						:
// 						<Link className="btn btn-danger btn-block">Log in to Enroll</Link>
// 					}
				    
// 				  </Card.Body>
// 				</Card>
// 			</Row>
// 		</Container>
// 	)
// }